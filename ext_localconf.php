<?php
defined('TYPO3_MODE') || die();

// xclass filelist controller to add diskusage button to file list module
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Filelist\Controller\FileListController::class] = [
    'className' => \MEDIAESSENZ\Diskusage\Controller\FileListController::class
];
