<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Diskusage\Utility;

use Doctrine\DBAL\DBALException;
use PDO;
use stdClass;
use RuntimeException;
use TYPO3\CMS\Backend\Routing\Exception\RouteNotFoundException;
use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\Folder;
use TYPO3\CMS\Core\Resource\FolderInterface;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Resource\ResourceStorageInterface;
use TYPO3\CMS\Core\Type\Bitmask\JsConfirmation;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class FalUtility
{
  /**
   * Return the combined parameter from the URL.
   *
   * @return string
   */
  protected static function getCombinedIdentifier(): string
  {
    // Fetch possible combined identifier.
    $combinedIdentifier = GeneralUtility::_GET('id');

    if ($combinedIdentifier) {

      // Fix a bug at the Core level: the "id" parameter is encoded again when translating file.
      // Add a loop to decode maximum 999 time!
      $semaphore = 0;
      $semaphoreLimit = 999;
      while (!self::isWellDecoded($combinedIdentifier) && $semaphore < $semaphoreLimit) {
        $combinedIdentifier = urldecode($combinedIdentifier);
        $semaphore++;
      }
    }

    return $combinedIdentifier ?? '';
  }

  /**
   * @param $combinedIdentifier
   *
   * @return bool
   */
  protected static function isWellDecoded($combinedIdentifier): bool
  {
    return preg_match('/.*:.*/', $combinedIdentifier) === 1;
  }

  /**
   * @return string
   */
  public static function getCombinedIdentifierOfCurrentFolder(): string
  {
    $combinedIdentifier = self::getCombinedIdentifier();

    if ($combinedIdentifier) {
      $folder = self::getFolderForCombinedIdentifier($combinedIdentifier);
    } else {
      $folder = self::getFirstAvailableFolder();
    }

    return $folder->getCombinedIdentifier();
  }

  /**
   * @param string $combinedIdentifier
   *
   * @return Folder
   */
  public static function getFolderForCombinedIdentifier($combinedIdentifier): Folder
  {
    // Code taken from FileListController.php
    $storage = GeneralUtility::makeInstance(ResourceFactory::class)->getStorageObjectFromCombinedIdentifier($combinedIdentifier);
    $identifier = substr($combinedIdentifier, strpos($combinedIdentifier, ':') + 1);
    if (!$storage->hasFolder($identifier)) {
      $identifier = $storage->getFolderIdentifierFromFileIdentifier($identifier);
    }

    // Retrieve the folder object.
    $folder = GeneralUtility::makeInstance(ResourceFactory::class)->getFolderObjectFromCombinedIdentifier($storage->getUid() . ':' . $identifier);

    // Disallow the rendering of the processing folder (e.g. could be called manually)
    // and all folders without any defined storage
    if ($folder && ($folder->getStorage()->getUid() == 0 || trim($folder->getStorage()->getProcessingFolder()->getIdentifier(),
          '/') === trim($folder->getIdentifier(), '/'))) {
      $storage = GeneralUtility::makeInstance(ResourceFactory::class)->getStorageObjectFromCombinedIdentifier($combinedIdentifier);
      $folder = $storage->getRootLevelFolder();
    }

    return $folder;
  }

  /**
   * @return Folder
   */
  protected static function getFirstAvailableFolder(): Folder
  {
    // Take the first object of the first storage.
    $storages = SystemUtility::getBackendUserAuthentication()->getFileStorages();
    $storage = reset($storages);
    if ($storage instanceof ResourceStorageInterface) {
      $folder = $storage->getRootLevelFolder();
    } else {
      throw new RuntimeException('Could not find any folder to be displayed.', 1444665954);
    }
    return $folder;
  }

  /**
   * @param Folder $folder
   * @param bool $hideReferences
   *
   * @return array
   * @throws RouteNotFoundException
   */
  public static function dir2Array($folder, $hideReferences = false): array
  {
    $a = [];
    $sizeOfFolder = 0;
    $exclude = ['.', '..', 'Thumb.db', '.htaccess', '_processed_', '_temp_'];
    /** @var UriBuilder $uriBuilder */
    $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);
    $files = $folder->getFiles();
    foreach ($files as $file) {
      if (!in_array($file->getName(), $exclude)) {
        $contentObject = new stdClass();
        $contentObject->name = $file->getName();
        $contentObject->path = $file->getPublicUrl();
        $contentObject->value = $file->getSize();
        $contentObject->references = self::getNumberOfReferences($file);
        $contentObject->combinedIdentifier = $file->getCombinedIdentifier();
        if ($contentObject->references === 0) {
          // delete
          if ($file->checkActionPermission('delete')) {
            $contentObject->deleteConfirmMessage = sprintf(SystemUtility::getLanguageService()->sL('LLL:EXT:core/Resources/Private/Language/locallang_core.xlf:mess.delete'), $file->getName());
            $contentObject->deleteCheck = SystemUtility::getBackendUserAuthentication()->jsConfirmation(JsConfirmation::DELETE);
            $contentObject->deleteUrl = (string)$uriBuilder->buildUriFromRoute('tce_file');
            $contentObject->deleteTitle = SystemUtility::getLanguageService()->sL('LLL:EXT:core/Resources/Private/Language/locallang_core.xlf:cm.delete');
          }
        }
        if ($contentObject->references === 0 || !$hideReferences) {
          $sizeOfFolder += $file->getSize();
          $a[] = $contentObject;
        }
      }
    }
    $subfolders = $folder->getSubfolders();
    foreach ($subfolders as $subfolder) {
      if (!in_array($folder->getName(), $exclude)) {
        $contentObject = new stdClass();
        $contentObject->name = $subfolder->getName();
        $contentObject->path = $subfolder->getPublicUrl();
        $subfolderFilesAndSize = self::dir2Array($subfolder, $hideReferences);
        $contentObject->value = $subfolderFilesAndSize['size'];
        $contentObject->combinedIdentifierMd5Int = GeneralUtility::md5int($subfolder->getCombinedIdentifier());
        $contentObject->uri = (string)$uriBuilder->buildUriFromRoute('file_DiskusageDiskusage', ['id' => $subfolder->getCombinedIdentifier()]);
        $sizeOfFolder += $subfolderFilesAndSize['size'];
        $contentObject->children = $subfolderFilesAndSize['files'];
        $a[] = $contentObject;
      }
    }

    return ['files' => $a, 'size' => $sizeOfFolder];
  }

    /**
     * Make reference count
     *
     * @param File|Folder $fileOrFolderObject Array with information about the file/directory for which to make the clipboard panel for the listing.
     * @return int
     * @throws DBALException
     */
  protected static function getNumberOfReferences($fileOrFolderObject): int
  {
    if ($fileOrFolderObject instanceof FolderInterface) {
      return 0;
    }
    // Look up the file in the sys_refindex.
    // Exclude sys_file_metadata records as these are no use references
    $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('sys_refindex');

    $result = $queryBuilder->count('*')
      ->from('sys_refindex')
      ->where(
//        $queryBuilder->expr()->eq('deleted', $queryBuilder->createNamedParameter(0, PDO::PARAM_INT)),
        $queryBuilder->expr()->eq(
          'ref_table',
          $queryBuilder->createNamedParameter('sys_file', PDO::PARAM_STR)
        ),
        $queryBuilder->expr()->eq(
          'ref_uid',
          $queryBuilder->createNamedParameter($fileOrFolderObject->getUid(), PDO::PARAM_INT)
        ),
        $queryBuilder->expr()->neq(
          'tablename',
          $queryBuilder->createNamedParameter('sys_file_metadata', PDO::PARAM_STR)
        )
      )
      ->execute()
      ->fetchColumn();
    return (int)$result;
  }

}
