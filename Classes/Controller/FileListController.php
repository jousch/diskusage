<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Diskusage\Controller;

use InvalidArgumentException;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Backend\Routing\Exception\RouteNotFoundException;
use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Backend\Template\Components\ButtonBar;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class FileListController extends \TYPO3\CMS\Filelist\Controller\FileListController
{
  /**
   * Registers the Icons into the docheader
   *
   * @throws InvalidArgumentException
   * @throws RouteNotFoundException
   */
  protected function registerAdditionalDocHeaderButtons(ServerRequestInterface $request): void
  {
    parent::registerAdditionalDocHeaderButtons($request);

    if ($this->getBackendUser()->isAdmin()) {

      $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);

      $buttonBar = $this->moduleTemplate->getDocHeaderComponent()->getButtonBar();

      $diskusageButton = $buttonBar->makeLinkButton()
        ->setHref((string)$uriBuilder->buildUriFromRoute(
          'file_DiskusageDiskusage',
          [
            'id' => $this->folderObject->getCombinedIdentifier(),
            'returnUrl' => $this->filelist->listURL(),
          ]
        ))
        ->setTitle($this->getLanguageService()->sL('LLL:EXT:diskusage/Resources/Private/Language/locallang.xlf:diskusage_backend_module.docheader.menu.action.main.title'))
        ->setIcon($this->iconFactory->getIcon('actions-viewmode-photos', Icon::SIZE_SMALL));
      $buttonBar->addButton($diskusageButton, ButtonBar::BUTTON_POSITION_LEFT, 4);
    }
  }
}
