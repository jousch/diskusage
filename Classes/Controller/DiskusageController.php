<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Diskusage\Controller;

use MEDIAESSENZ\Diskusage\Utility\SessionUtility;
use MEDIAESSENZ\Diskusage\Utility\SystemUtility;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use RuntimeException;
use TYPO3\CMS\Backend\Template\Components\ButtonBar;
use TYPO3\CMS\Backend\Template\ModuleTemplate;
use TYPO3\CMS\Backend\Template\ModuleTemplateFactory;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Core\Resource\Exception\InsufficientFolderAccessPermissionsException;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;
use TYPO3Fluid\Fluid\View\ViewInterface;

class DiskusageController
{
    /**
     * @var IconFactory
     */
    protected IconFactory $iconFactory;

    /**
     * @var ResourceFactory
     */
    protected ResourceFactory $resourceFactory;

    /**
     * @var ModuleTemplateFactory
     */
    protected ModuleTemplateFactory $moduleTemplateFactory;

    /**
     * @var ModuleTemplate|null
     */
    protected ?ModuleTemplate $moduleTemplate = null;

    /**
     * @var ResponseFactoryInterface
     */
    protected ResponseFactoryInterface $responseFactory;

    /**
     * @var ViewInterface|null
     */
    protected ?ViewInterface $view = null;

    public function __construct(
        IconFactory $iconFactory,
        ResourceFactory $resourceFactory,
        ModuleTemplateFactory $moduleTemplateFactory,
        ResponseFactoryInterface $responseFactory
    ) {
        $this->iconFactory = $iconFactory;
        $this->resourceFactory = $resourceFactory;
        $this->moduleTemplateFactory = $moduleTemplateFactory;
        $this->responseFactory = $responseFactory;
    }

    /**
     * @param ServerRequestInterface $request the current request
     *
     * @return ResponseInterface the response with the content
     * @throws InsufficientFolderAccessPermissionsException
     */
    public function handleRequest(ServerRequestInterface $request): ResponseInterface
    {
        $this->moduleTemplate = $this->moduleTemplateFactory->create($request);
        $parsedBody = $request->getParsedBody();
        $queryParams = $request->getQueryParams();

        $target = ($combinedIdentifier = $parsedBody['target'] ?? $queryParams['target'] ?? '');
        $returnUrl = GeneralUtility::sanitizeLocalUrl($parsedBody['returnUrl'] ?? $queryParams['returnUrl'] ?? '');
        $folderObject = null;

        SessionUtility::getInstance()->set('combinedIdentifierOfCurrentFolder', $target);

        // create the folder object
        if ($combinedIdentifier) {
            $folderObject = $this->resourceFactory->getFolderObjectFromCombinedIdentifier($combinedIdentifier);
        }
        // Cleaning and checking target directory
        if (!$folderObject) {
            throw new RuntimeException(
                SystemUtility::getLanguageService()->sL('LLL:EXT:filelist/Resources/Private/Language/locallang_mod_file_list.xlf:paramError')
                . ': ' . SystemUtility::getLanguageService()->sL('LLL:EXT:filelist/Resources/Private/Language/locallang_mod_file_list.xlf:targetNoDir'),
                1294586845);
        }
        if ($folderObject->getStorage()->getUid() === 0) {
            throw new InsufficientFolderAccessPermissionsException(
                'You are not allowed to access folders outside your storages',
                1375889838
            );
        }

        $this->moduleTemplate->getDocHeaderComponent()->setMetaInformation([
            'combined_identifier' => $folderObject->getCombinedIdentifier(),
        ]);

        $buttonBar = $this->moduleTemplate->getDocHeaderComponent()->getButtonBar();

        // Back
        if ($returnUrl) {
            $backButton = $buttonBar->makeLinkButton()
                ->setHref($returnUrl)
                ->setTitle(SystemUtility::getLanguageService()->sL('LLL:EXT:core/Resources/Private/Language/locallang_core.xlf:labels.goBack'))
                ->setIcon($this->iconFactory->getIcon('actions-view-go-back', Icon::SIZE_SMALL));
            $buttonBar->addButton($backButton, ButtonBar::BUTTON_POSITION_LEFT, 1);
        }

        // Rendering of the output via fluid
        $this->initializeView();

        $this->view->assign('target', $target);

        return $this->htmlResponse(
            $this->moduleTemplate->setContent($this->view->render())->renderContent()
        );
    }

    protected function initializeView(): void
    {
        $this->view = GeneralUtility::makeInstance(StandaloneView::class);
        $this->view->setTemplatePathAndFilename(GeneralUtility::getFileAbsFileName(
            'EXT:diskusage/Resources/Private/Templates/BackendModule/Default.html'
        ));
    }

    /**
     * Generate a response by either the given $html or by rendering the module content.
     *
     * @param string $html
     * @return ResponseInterface
     */
    protected function htmlResponse(string $html): ResponseInterface
    {
        $response = $this->responseFactory
            ->createResponse()
            ->withHeader('Content-Type', 'text/html; charset=utf-8');

        $response->getBody()->write($html);
        return $response;
    }
}
