import Vue from 'vue';
import ECharts from 'vue-echarts';
import PortalVue from 'portal-vue';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/legend';
import 'echarts/lib/component/title';
import { notification, loader, ajaxUrls } from './utilities/Typo3Environment';
import ContextMenu from './components/ContextMenu';
import Diskusage from './components/Diskusage';

Vue.use(PortalVue);

Vue.component('chart', ECharts);
Vue.component('context-menu', ContextMenu);

document.addEventListener('DOMContentLoaded', () => {
  new Vue({
    data: {
      title: 'Disk Usage',
      folder: '/',
      diskusage: [],
      hideReferences: false,
    },
    created() {
      this.fetchMainData();
    },
    methods: {
      fetchMainData() {
        loader.start();
        fetch(ajaxUrls.diskusage_default).then(response => response.json()).then((data) => {
          this.folder = data.folder;
          this.diskusage = data.diskusage;
          this.hideReferences = data.hideReferences;
          // eslint-disable-next-line no-restricted-globals
          loader.finish();
        }).catch((error) => {
          notification.error('Backend module controller feedback', error);
        });
      }
    },
    render(h) {
      return h(Diskusage, {
        props: {
          title: this.title,
          folder: this.folder,
          diskusage: this.diskusage,
          hideReferences: this.hideReferences,
        }
      });
    }
  }).$mount('#diskusage-backend-module-content');

});
