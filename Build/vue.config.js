module.exports = {
  outputDir: '../Resources/Public',
  publicPath: '/typo3conf/ext/diskusage/Resources/Public/',
  filenameHashing: false,
  productionSourceMap: false,
  runtimeCompiler: false,
  transpileDependencies: [
    'vue-echarts',
    'resize-detector'
  ],
  configureWebpack: {
    entry: {
      main: './src/main.js'
    },
    output: {
      filename: process.env.VUE_CLI_MODERN_BUILD ? 'JavaScript/[name].js' : 'JavaScript/[name]-legacy.js',
      chunkFilename: process.env.VUE_CLI_MODERN_BUILD ? 'JavaScript/[name].js' : 'JavaScript/[name]-legacy.js',
    },
  },
  chainWebpack: (config) => {
    config.entryPoints.delete('app');
    config.module.rule('images').use('url-loader').loader('url-loader').tap((options) => {
      options.fallback.options.name = 'Images/[name].[ext]';
      return options;
    });
    config.module.rule('svg').use('file-loader').loader('file-loader').tap((options) => {
      options.name = 'Images/[name].[ext]';
      return options;
    });
    config.module.rule('media').use('url-loader').loader('url-loader').tap((options) => {
      options.fallback.options.name = 'Media/[name].[ext]';
      return options;
    });
    config.module.rule('fonts').use('url-loader').loader('url-loader').tap((options) => {
      options.fallback.options.name = 'Fonts/[name].[ext]';
      return options;
    });
    config.plugins.delete('html');
    config.plugins.delete('preload');
    config.plugins.delete('prefetch');
  },

  css: {
    extract: {
      filename: 'Css/[name].css',
      chunkFilename: 'Css/[name].css',
    },
  },

};
