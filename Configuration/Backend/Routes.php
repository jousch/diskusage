<?php

return [
  // Show disk usage
  'file_diskusage' => [
    'path' => '/file/diskusage',
    'target' => \MEDIAESSENZ\Diskusage\Controller\DiskusageController::class . '::handleRequest'
  ],

];
